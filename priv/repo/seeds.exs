# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     UsersLottery.Repo.insert!(%UsersLottery.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias UsersLottery.Users.User

defmodule UsersSeeder do
  @insert_chunk_size 10_000

  def seed(total) when total >= 1,
    do:
      1..total
      |> Enum.map(fn _ -> generate_user() end)
      |> Enum.chunk_every(@insert_chunk_size)
      |> Enum.each(&insert_users/1)

  defp generate_user(),
    do: %{
      points: 0,
      inserted_at: {:placeholder, :now},
      updated_at: {:placeholder, :now}
    }

  defp insert_users(users) do
    with now <- NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second) do
      UsersLottery.Repo.insert_all(User, users, placeholders: %{now: now})
    end
  end
end

UsersSeeder.seed(1_000_000)
