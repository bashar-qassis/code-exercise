defmodule UsersLottery.Users do
  alias UsersLottery.Repo
  alias UsersLottery.Users.User

  import Ecto.Query, only: [where: 3, limit: 2, update: 2]

  @max_number 100
  @default_opts %{min_points: nil, limit: 2}

  def get(opts) do
    opts = Enum.into(opts, @default_opts)

    User
    |> maybe_filter_by_points(opts.min_points)
    |> maybe_limit(opts.limit)
    |> Repo.all()
  end

  def update_users_points(),
    do:
      User
      |> update(
        set: [points: fragment("random() * ?", @max_number), updated_at: fragment("NOW()")]
      )
      |> Repo.update_all([])

  # dodging an unnecessary where filter if the min_points are not set (default value) or invalid
  defp maybe_filter_by_points(query, min_points) when min_points < 0 or is_nil(min_points),
    do: query

  defp maybe_filter_by_points(query, min_points),
    do: where(query, [user], user.points > ^min_points)

  defp maybe_limit(query, limit) when limit < 1 or is_nil(limit), do: query
  defp maybe_limit(query, limit), do: limit(query, ^limit)
end
