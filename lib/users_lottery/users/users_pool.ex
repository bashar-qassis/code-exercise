defmodule UsersLottery.UsersPool do
  use GenServer

  alias UsersLottery.Users

  @max_number 100

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  @impl true
  def init(_init_arg) do
    timer = Process.send_after(self(), :tick, get_config(:update_interval_ms))

    {:ok, %{timestamp: nil, min_number: :rand.uniform(@max_number), timer: timer}}
  end

  @impl true
  def handle_call(:pick, _from, state) do
    users = Users.get(min_points: state.min_number, limit: get_config(:users_limit))

    {
      :reply,
      %{users: users, timestamp: state.timestamp},
      %{state | timestamp: NaiveDateTime.utc_now()}
    }
  end

  @impl true
  def handle_info(:tick, state) do
    with _result <- Users.update_users_points(),
         timer <- Process.send_after(self(), :tick, get_config(:update_interval_ms)) do
      {:noreply, %{state | timer: timer, min_number: :rand.uniform(@max_number)}}
    end
  end

  defp get_config(), do: Application.get_env(:users_lottery, __MODULE__)
  defp get_config(key), do: get_config()[key]
end
