defmodule UsersLottery.Repo do
  use Ecto.Repo,
    otp_app: :users_lottery,
    adapter: Ecto.Adapters.Postgres
end
