defmodule UsersLotteryWeb.UserController do
  use UsersLotteryWeb, :controller

  alias UsersLottery.UsersPool

  def index(conn, _params), do: render(conn, :index, GenServer.call(UsersPool, :pick))
end
