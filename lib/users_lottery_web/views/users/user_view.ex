defmodule UsersLotteryWeb.UserView do
  use UsersLotteryWeb, :view

  def render("index" <> _, %{users: users, timestamp: timestamp} = __params),
    do: %{
      users: render_many(users, __MODULE__, "show_user", as: :user),
      timestamp: timestamp
    }

  def render("show_user" <> _, %{user: user} = _params),
    do: %{
      id: user.id,
      points: user.points
    }
end
