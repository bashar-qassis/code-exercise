defmodule UsersLotteryWeb.Router do
  use UsersLotteryWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", UsersLotteryWeb do
    pipe_through :api
  end

  scope "/", UsersLotteryWeb do
    pipe_through :api

    get "/", UserController, :index
  end
end
