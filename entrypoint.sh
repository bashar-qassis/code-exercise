#!/bin/bash
# Docker entrypoint script.

# Wait until Postgres is ready
echo "Testing if Postgres is accepting connections. {$DB_HOST} {$DB_PORT} ${DB_USERNAME}"
while ! pg_isready -q -h $DB_HOST -p $DB_PORT -U $DB_USERNAME; do
  echo "$(date) - waiting for database to start"
  sleep 2
done

# Create, migrate, and seed database if it doesn't exist.
if [[ -z $(psql -Atqc "\list $DB_DATABASE") ]]; then
  echo "Database $DB_DATABASE does not exist. Creating..."
  mix ecto.setup
  echo "Database $DB_DATABASE created."
fi

exec mix phx.server
