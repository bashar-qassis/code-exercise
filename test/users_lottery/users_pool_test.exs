defmodule UsersLottery.UsersPoolTest do
  use UsersLottery.DataCase

  @min_number 0
  @max_number 100

  setup do
    # Restart the application before every test to reset its state
    :ok = Application.stop(:users_lottery)
    :ok = Application.start(:users_lottery)
  end

  test "Empty UsersPool state is valid" do
    pid = Process.whereis(UsersLottery.UsersPool)
    %{min_number: min_number, timestamp: timestamp} = :sys.get_state(pid)

    assert min_number >= @min_number
    assert min_number <= @max_number
    assert is_nil(timestamp)
  end

  test "UsersPool state timestamp changes after calling pick" do
    pid = Process.whereis(UsersLottery.UsersPool)

    %{timestamp: timestamp_1} = :sys.get_state(pid)
    GenServer.call(UsersLottery.UsersPool, :pick)

    %{timestamp: timestamp_2} = :sys.get_state(pid)
    GenServer.call(UsersLottery.UsersPool, :pick)
    %{timestamp: timestamp_3} = :sys.get_state(pid)

    assert is_nil(timestamp_1)
    assert not is_nil(timestamp_2)
    assert is_struct(timestamp_2, NaiveDateTime)
    assert not is_nil(timestamp_3)
    assert is_struct(timestamp_3, NaiveDateTime)
    assert timestamp_1 != timestamp_2 != timestamp_3
  end

  test "UsersPool returns users with point greater than min_number when calling pick" do
    %{users: users} = GenServer.call(UsersLottery.UsersPool, :pick)

    pid = Process.whereis(UsersLottery.UsersPool)
    %{min_number: min_number} = :sys.get_state(pid)

    Enum.each(users, fn user ->
      assert user.points > min_number
    end)
  end

  test "UsersPool state min_number changes after timer and is valid" do
    pid = Process.whereis(UsersLottery.UsersPool)

    %{min_number: prev_min_number} = :sys.get_state(pid)

    # wait for UsersPool to update the min_number
    Process.sleep(get_config(:update_interval_ms) + 200)

    %{min_number: min_number} = :sys.get_state(pid, :infinity)

    assert min_number >= @min_number
    assert min_number <= @max_number
    assert prev_min_number != min_number
  end

  defp get_config(key), do: Application.get_env(:users_lottery, UsersLottery.UsersPool)[key]
end
