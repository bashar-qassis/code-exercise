import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :users_lottery, UsersLottery.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "users_lottery_dev",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :users_lottery, UsersLotteryWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "+o0qPqJ6o3Gj8LuMM5aDsYTgy8+t772zyOKyTbgxdvHKy0AMZ4diFL1+QAeMyRJJ",
  server: false

config :users_lottery, UsersLottery.UsersPool,
  users_limit: 2,
  update_interval_ms: 1_000

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
