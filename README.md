# UsersLottery

UsersLottery is a weird application that gives users random `points` from 0-100. The application has one public API `/` that chooses 2 users with `points` above a random number from 0-100.

Every minute, the users points are updated randomly in addition to the evaluation number that chooses the users.

## Environment Setup

* Create a `.env` file based on the `.env.example` template.
* Install Docker (optional).
* Install erlang 25 & elixir v1.13.4-otp-25. If you're using `asdf` this can be done by simply executing `asdf install`.


## Starting the application

To start your Phoenix server:

  * Make sure Docker is running.
  * Run `docker compose up` and you're good to go. This will setup a postgres container in addition to bootstrapping and starting the phoenix application.

Alternatively, If you prefer to go oldschool:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

---

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
